/**
 * Created by edgera on 9/30/2014.
 */
// potential creeps
var TowerEnum = Object.freeze(
    {
        GENERIC: Tower,
        RED: 1,
        BLUE: 2
    }
);


// Constructor
function Tower(x,y) {
    this.x = x;
    this.y = y;
    this.range = 2;
    this.cooldown = 0; //ticks between shots
    this.charge = 100;

    this.cost = 50;

    this.sprite = null;
    this.stage = null;
}



// class methods
Tower.prototype.create_sprite = function(target_stage) {

    if (typeof createjs === 'undefined') {
        return {};
    }
    console.log('drawing dope tower@' +this.x + ' '+this.y);
    var circle = new createjs.Shape();
    circle.graphics.beginFill("red").drawCircle(0, 0, grid_height/2);
    circle.x = Math.round(this.x * grid_width);
    circle.y = Math.round(this.y * grid_height);
    //circle.addEventListener("click", function(e) { alert("some cool tower"); });
    target_stage.addChild(circle);
    //target_stage.update();
    this.sprite = circle;
    this.stage = target_stage;
    return circle;
};

//@args array of creep objects
Tower.prototype.shootAt = function(creeps){
    //TODO real targeting
    for(var i = 0 ; i < creeps.length;i++){
        var x2 =(creeps[i].x - this.x)*(creeps[i].x - this.x);
        var y2 =(creeps[i].y - this.y)*(creeps[i].y - this.y);

        if(x2+y2 < this.range*this.range &&  this.cooldown == 0 &&creeps[i].health>0){
            // found target.
            // todo just remove health for now. in future do different affects.
            creeps[i].health -= 50;
            this.cooldown = this.charge;
            if (typeof this.sprite != 'undefined') {
                this.animateShot();
            }

            break;
        }
    }

    if(this.cooldown>0) {
        this.cooldown -= 1;
    }



}


//bad hacky show active towers
Tower.prototype.animateShot = function(){
    if(this.sprite == null){
        return;
    }
    var _this = this;
    _this.sprite.graphics.c();//tiny clear

    this.sprite.graphics.beginFill("#FF00A0").drawCircle(0, 0, grid_width/2+5);
    this.stage.update();
    setTimeout(function () {
        _this.sprite.graphics.c();//tiny clear
        _this.sprite.graphics.beginFill("red").drawCircle(0, 0, grid_width/2);
        _this.stage.update();
    }, 5*1000/target_tps);
}

Tower.fromJson = function(k,json){
    console.log(json);
    return new Tower(json.x,json.y);
};

Tower.prototype.toJSON = function(){

    var json = {
        x:this.x,
        y:this.y,
        range:this.range,
        charge:this.charge
    };
    return json;
}

// export the class
module.exports = Tower;