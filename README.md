# README #

This README documents the steps necessary to get the application up and running.

### What is this repository for? ###

* Creating a Tower Defense game
* Creating an AI Platform

### How do I get set up? ###

* Make sure node is installed.
* run `npm install` to acquire 3rd party packages
* run `node server.js`
* visit page in browser

### How do I run the tests? ###

We use Jasmine as our testing framework. To execute the Jasmine test suite...

* run `node server.js`
* navigate to localhost:4004/jasmine/SpecRunner.html in a browser.

For details on how to write Jasmine tests, see http://jasmine.github.io/2.0/introduction.html.
