/**
 * Created by Robert.Edge on 10/30/2014.
 */

var io = require('socket.io-client')
var clientSocket = io.connect('http://localhost:4004');


//serverSocket.on('send', on_send);
//serverSocket.on('tower_buy', on_tower_buy);
//serverSocket.on('tower_sell', on_tower_sell);
//serverSocket.on('creep_buy', on_creep_buy);


var my_id;
clientSocket.on('onconnected', function (data) {
    // Note that the data is the object we sent from the server, as is. So we can assume its id exists.
    console.log('Connected successfully to the socket.io server. My server side ID is ' + data.id);
    my_id = data.id;
});

clientSocket.emit('join', function (response) {
    console.log('Joined room ' + response.roomId + ' as ' + response.role);
});

clientSocket.on('gamestart', function (json_gamestate) {
    console.log(json_gamestate);
    think_and_action(json_gamestate);
});

clientSocket.on('wave_data', function (data){
    console.log('wave happende')
});



function think_and_action(json_gamestate){
    console.log('thinking and making action..');
    console.warn('uhhh');
    var my_land = json_gamestate.lands[my_id];


    //can afford tower
    while(my_land.gold>100) {
        var best_spots = find_best_corner(json_gamestate);
        //check that spots is valid
        var x = best_spots[0][0];
        var y = best_spots[0][1];
        var json = {
            x:x,
            y:y
        };
        clientSocket.emit('tower_buy',json);
        my_land.gold -= 100;
    }
}

function find_best_corner(json_gamestate){

    console.log('finding best corner');
    var map = json_gamestate.map
    var grid = new Array();
    var my_towers = json_gamestate.lands[my_id].towers;

    for (var i=0;i<map.width;i++) {
        grid[i]=new Array();
        for (var j=0;j<map.height;j++) {
            grid[i][j]=0;
        }
    }

    //starts at i =2 to ignore start on edge of map. ignor last coord as well
    for( var i = 2 ; i < map.path.length -3 ; i+=2){
        var x = map.path[i]-.5
        var y = map.path[i+1]-.5

        //simple 3x3 filter
        if(x-1 >=0 && y-1>=0){
            grid[x-1][y-1]+=1;
        }
        if( y-1>=0){
            grid[x][y-1]+=1;
        }
        if(x+1 <map.width && y-1>=0){
            grid[x+1][y-1]+=1;
        }
        if(x-1>=0){
            grid[x-1][y]+=1;
        }
        if(x+1<map.width){
            grid[x+1][y]+=1;
        }
        if(x-1 >=0 && y+1<map.height){
            grid[x-1][y+1]+=1;
        }
        if(y+1<map.height){
            grid[x][y+1]+=1;
        }
        if(x+1 <map.width  && y+1<map.height){
            grid[x+1][y+1]+=1;
        }

    }

    var vis = '';

    var best_spots = [];
    var best_score = 0;
    for (j=0;j<map.height;j++) {
        for (i=0;i<map.width;i++) {

            if(tower_exists_at(i,j,my_towers)){
                vis= vis+ (' '+x);
                continue;
            }

            if(grid[i][j]>best_score){
                best_spots = [];
                best_score = grid[i][j];
                best_spots.push([i,j]);
            }else if(grid[i][j]=best_score){
                best_spots.push([i,j]);
            }
            vis= vis+ (' '+grid[i][j]);
        }
        vis+='\n';
    }
    return best_spots;

}

function tower_exists_at(x,y,towers){
    for(var i =0 ;i < towers.length ; i++){
        var t = towers[i];
        if(t.x == x && t.y == y) {
            return true;
        }
    }
    return false;
}