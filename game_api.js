/**
 * server socket handlers and emitters here. The game API definition.
 */

var UUID = require('node-uuid');
var engine = require('./core/engine');
var gamestateProto = require('./core/gamestate');


var Tower = require('./core/tower');

var creepProto = require('./core/creep');
var player1 = 0;

// holds all gamestates. key = room uuid
var games = {};


module.exports = function(io){
    var serverSocket; //not sure if this is appropriate scoping technique

    return function (socket){
        serverSocket = socket;
        // Generate a new UUID, looks something like: 5b2ca132-64bd-4513-99da-90e838ca47d1
        serverSocket.userid = UUID();
        console.log('\t socket.io:: player ' + serverSocket.userid + ' connected');
        // tell the player they connected, giving them their id
        serverSocket.emit('onconnected', { id: serverSocket.userid });
        // client emits 'join' event on page load
        serverSocket.on('join', on_join );
        serverSocket.on('disconnect', on_disconnect);

        /**
         *  Game API
         */
        serverSocket.on('send', on_send);
        serverSocket.on('tower_buy', on_tower_buy);
        serverSocket.on('tower_sell', on_tower_sell);
        serverSocket.on('creep_buy', on_creep_buy);
        serverSocket.on('request_gamestate', on_request_gamestate);

    };

    // event stuff
    function on_request_gamestate(data){
        io.sockets.emit('gamestate', games[serverSocket.room].toJson());
    }


    function on_join(callback) {
        //find room


        if (!player1) {
            // host the room
            console.log(serverSocket.userid + ': is waiting for connect');
            serverSocket.room = serverSocket.userid;
            serverSocket.join(serverSocket.room);
            player1 = serverSocket.userid;
            var role = "HOST";
        }
        else {
            // join available room
            console.log(serverSocket.userid + ': is joining the waiting player.');
            serverSocket.join(player1);
            var room = serverSocket.room = player1;
            var role = "NON-HOST";
            //map is scaled by the creep
            var map = {
                width: 10,
                height: 15,
                path: [
                    0, .5,
                    .5,  .5,
                    1.5,  .5,
                    1.5, 1.5,
                    2.5, 1.5,
                    3.5, 1.5,
                    4.5, 1.5,
                    4.5, 2.5,
                    5.5, 2.5,
                    6.5, 2.5,
                    7.5, 2.5,
                    8.5, 2.5,
                    9.5, 2.5,
                    10, 2.5
                ]
            };
            games[room] = new gamestateProto([player1,serverSocket.userid],map);

            io.sockets.in(serverSocket.room).emit('gamestart', games[serverSocket.room].toJson());

            player1 = 0;

        }

        callback({role: role, roomId: serverSocket.room});
    }

    function on_disconnect (data) {
        // Useful to know when someone disconnects
        console.log('\t socket.io:: client disconnected ' + serverSocket.userid);
    }

    function on_send(data){
        console.log('haha server side and wave process happened');
        io.sockets.in(serverSocket.room).emit('wave_data', games[serverSocket.room].toJson());

        //figure out how to have a server-side known truth. (simulate the wave)
        var gamestate = games[serverSocket.room];

        while(!engine.isWaveFinished(gamestate)) {// todo not robust
            gamestate = engine.tick(gamestate);
        };

        engine.do_end_of_wave(gamestate);
    }

    function on_creep_buy(data) {
        var creep = creepProto.fromJson(data);
        var player = serverSocket.userid;
        var gamestate = games[serverSocket.room];
        engine.buy_creep(creep,player,gamestate);
    };

    function on_tower_buy(json_tower) {
        var tower = new Tower(json_tower.x,json_tower.y);
        // games[serverSocket.room].lands[serverSocket.userid].towers.push(tower);
        var player = serverSocket.userid;
        var gamestate = games[serverSocket.room];
        engine.buy_tower(tower,player,gamestate);
    }

    function on_tower_sell(data) {
        var towerAction = JSON.parse(data);
        console.log("tower_sell:"+data);
    }
};
