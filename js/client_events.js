/**
 * put client socket event handlers here
 */

function do_wave(json_gamestate){
    console.log('doing a wave');
    // 1 lock input until the end of the wave. TODO
    // 2 convert the gamestate json representation to full objects.
    gamestate = Gamestate.fromJson(json_gamestate);
    // 2a add all of the objects to the appropriate stages.
    // 2b remove all invalid towers (only happens if player manually added by modifying clientside js)
    draw_wave_updates(gamestate);

    // 3 process each tick of the wave. We need to do a tick-based calculation
    //   to ensure that the client and server results match, ie a time-based process
    //   would potentially result in different creeps being targeted because of how
    //   the wave distro is made discrete at different timesteps.
    // 3a becaues we do not want to render the creep wves as fast as we can compute them,
    //    we wil use a interupt timer to update the stages every t seconds.
    run_wave(gamestate);

    // we need to wait for run_wave to complete before executing checks.
    // run_wave() will be responsible for calling checks when it is complete.

};

function draw_wave_updates(gamestate){
    for(var i = 0 ; i < gamestate.players.length; i++ ){
        var player= gamestate.players[i];
        var land = gamestate.lands[player];
        // make sure all tower sprites are rendered.
        for(var j = 0; j<land.towers.length; j++){
            var tower = land.towers[j];
            if (tower.sprite == null){
                tower.create_sprite(stages[player]);
            }
        }
        stages[player].update();
    }
}

var target_tps = 30;

function run_wave(gamestate){
    Engine.tick(gamestate);
    update_stats_display(gamestate);

    if(!Engine.isWaveFinished(gamestate)) {
        setTimeout(function () {
            run_wave(gamestate);
        }, 1000/target_tps);
    }else{
        end_wave(gamestate);
    }
}

function end_wave(gamestate){
    console.log('finished doing a wave');
    Engine.do_end_of_wave(gamestate);
    update_stats_display(gamestate);

    //check if the game is over
    if(Engine.numberOfRemainingLands(gamestate)==1){
        alert('game end');
    }

}

function update_stats_display(gamestate){
    for(var i = 0 ; i < gamestate.players.length; i++ ){
        var player= gamestate.players[i];
        update_stats(stages[gamestate.players[i]],gamestate.lands[player]);
        stages[gamestate.players[i]].update();
    }
}


function buyCreep() {
    var creep = new Creep(.1,100);

    if(!Engine.buy_creep(creep, my_id, gamestate)){
        console.log('client-side creep not purchased');
        return; // TODO visual indicator
    }
    update_stats(stages[my_id],gamestate.lands[my_id]);
    console.log(creep.toJson());
    clientSocket.emit('creep_buy',creep.toJson() );
}



function buyTower(x,y,stage) {
    var tower = new Tower(x, y);

    if(!Engine.buy_tower(tower, my_id, gamestate)){
        console.log('client-side tower not purchased');
        return; // TODO visual indicator
    }
    tower.create_sprite(stage);
    update_stats(stages[my_id],gamestate.lands[my_id]);
    clientSocket.emit('tower_buy', tower.toJSON());
}





//send TODO move this funcitonality into the server in a timed loop.
function send() {
    clientSocket.emit('send', function () {
        console.log('clientsendd clicked callback');
    });
};



