/**
 * client side initialization
 */

var grid_width = 30;
var grid_height = 30;
var num_grid_rows = 10;
var num_grid_cols = 15;

var my_id; //set in onconnected()

var gamestate;
var context = {};
var stages = {};
var clientSocket;

// called on page load
// initialize global vars and attach client's socket event listeners
function clientInit() {
    // create client side communicator
    clientSocket = io.connect('/');
    attachClientSocketEvents();
}

// attach event listeners to socket events
function attachClientSocketEvents() {
    clientSocket.on('onconnected', function (data) {
        // Note that the data is the object we sent from the server, as is. So we can assume its id exists.
        console.log('Connected successfully to the socket.io server. My server side ID is ' + data.id);
        my_id = data.id;
    });

    clientSocket.emit('join', function (response) {
        console.log('Joined room ' + response.roomId + ' as ' + response.role);
    });

    clientSocket.on('gamestart', function (json_gamestate) {
        init_each_land_visual(json_gamestate);
    });

    clientSocket.on('wave_data', do_wave);

}

/*
* The intention of this function is to create each player's game board and display it
* - creates the canvas,
* - places it in the DOM,
* - creates the interactions on the background,
* - draws the path
*/
function init_each_land_visual(json_gamestate) {
    gamestate  = Gamestate.fromJson(json_gamestate);
    console.log('cerated gamesatae');
    var map_div = document.getElementById('maps');
    console.log('init_each_land');
    for (var i = 0 ; i < gamestate.players.length; i ++){
        var player = gamestate.players[i];
        var land = gamestate.lands[player];
        console.log("init: player:"+player + " - " + land.life+" - " +land.gold +'g - ' + land.income);

        var canvas = create_canvas(land);
        map_div.appendChild(canvas);
        var divider = document.createElement('div');
        divider.setAttribute('class', 'divider');
        map_div.appendChild(divider);

        stages[player] = new createjs.Stage(player);
        context[player] = document.getElementById(player).getContext("2d");

        create_bg(stages[player], num_grid_rows, num_grid_cols);
        draw_path(stages[player], gamestate.map['path']);
        create_stats(stages[player],land);
        stages[player].update();
    }
};

/* creates the canvas for each player's land*/
function create_canvas(land){
    var c = document.createElement('canvas');
    c.setAttribute('id', land.player);
    c.setAttribute('width', 400);
    c.setAttribute('height', 600);

    return c ;
};


function create_bg(stage, cols, rows) {
    stage.enableMouseOver();
    var padding = 0;
    for (var i = 0; i < rows * cols; i++) {
        var s = new createjs.Shape();
        s.overColor = "#997777";
        s.outColor = "#443333";
        s.pathColor = "#cc9977";
        s.graphics.beginFill(s.outColor).drawRect(0, 0, grid_width, grid_height).endFill();
        s.x = (grid_width + padding) * (i % cols);
        s.y = (grid_height + padding) * (i / cols | 0);
        s.on('mouseover', function (event) {
            var target = event.target;
            target.graphics.clear().beginFill(target.overColor).drawRect(0, 0, grid_width, grid_height).endFill();
            stage.update();
        });
        s.on('mouseout', function (event) {
            var target = event.target;
            target.graphics.clear().beginFill(target.outColor).drawRect(0, 0, grid_width, grid_height).endFill();
            stage.update();
        });

        if (stage === stages[my_id]) {
            s.on('mousedown', function (e) {
                    var x = Math.floor(e.stageX / grid_width)  +.5;
                    var y = Math.floor(e.stageY / grid_width)  +.5;
                    buyTower(x,y,stage);
                }
            );
        }
        stage.addChild(s);
    }

}

function draw_path(stage, path) {
    for (var i = 0; i < path.length - 3; i += 2) {
        x0 = path[i]*grid_width;
        y0 = path[i + 1]*grid_height;
        x1 = path[i + 2]*grid_width;
        y1 = path[i + 3]*grid_height;
        var line = new createjs.Shape();
        line.graphics.moveTo(x0, y0).setStrokeStyle(1).beginStroke("#00ff00").lineTo(x1, y1);
        stage.addChild(line);
    }
    stage.update();
}

function create_stats(stage,land){

    var container = new createjs.Container();
    container.x = grid_width * (num_grid_rows);
    container.y = grid_height * (num_grid_cols);
    container.name = 'stats';
    container.setBounds(0, 0, 2*grid_width, 3*grid_height);
    // life
    var text = new createjs.Text();
    text.set({
        text:'life:'+land.life,
        name:'life'
    });
    container.addChild(text);
    //gold
    text = new createjs.Text();
    text.set({
        text:'gold:'+land.gold,
        name:'gold',
        y:grid_height
    });
    container.addChild(text);
    //income
    text = new createjs.Text();
    text.set({
        text:'income:'+land.income,
        name:'income',
        y:grid_height*2
    });
    container.addChild(text);



    stage.addChild(container);

}

function update_stats(stage,land){
    var stats = stage.getChildByName('stats');
    var life_text = stats.getChildByName('life');
    life_text.text = 'life:'+land.life;
    var gold_text = stats.getChildByName('gold');
    gold_text.text = 'gold:'+land.gold;
    var gold_text = stats.getChildByName('income');
    gold_text.text = 'income:'+land.income;
    stage.update();
}

